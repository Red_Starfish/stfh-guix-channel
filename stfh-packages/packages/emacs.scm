(define-module (stfh-packages packages emacs)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix download)
  #:use-module (guix build-system emacs)
  #:use-module (gnu packages emacs-xyz))

(define-public emacs-good-scroll
  (package
    (name "emacs-good-scroll")
    (version "20210820.633")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://melpa.org/packages/good-scroll-"
                           version ".tar"))
       (sha256
        (base32
         "1phhlx1r68f5yh6sml1q27av7x310vvfr0krnglii2p5bqi43h4d"))))
    (build-system emacs-build-system)
    (home-page "https://github.com/io12/good-scroll.el")
    (synopsis "Attempt at good pixel-based smooth scrolling in Emacs")
    (description "@code{emacs-good-scroll} implements smooth scrolling by pixel
lines. It attempts to improve upon pixel-scroll-mode by adding
variable speed.")
    (license license:x11)))
